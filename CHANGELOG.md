# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]



## [1.1.0]

### Added
- DummyTransformation as default transformation for ETL instances, to 
  make old ETL subclasses compatible with new release

### Changed
- ETL class is not abstract and should not be subclassed
- ETL class has now a transformation attribute that points to a Transformation class, 
  where data from the extractor are transformed in order to be fed into the loader.
  The transformation's ``transform`` method replaces the ETL one. Backcompatibilty is endured. 

### Fixed


## [1.0.0]
### Added
- basic ETL abstract class
- Extractor subclasses for CSV, HTML, Sparql, SQL, XLS
- Loaders for CSV, Django, ES
- extractors and loaders use Pandas

