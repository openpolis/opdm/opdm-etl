import os

from setuptools import setup, find_packages


def read_file(filename):
    """Read a file into a string"""
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ''


setup(
    name='opdm-etl',
    version=__import__('opdmetl').__version__,
    author='Guglielmo Celata',
    author_email='guglielmo@openpolis.it',
    packages=find_packages(),
    include_package_data=True,
    url='http://gitlab.depp.it/openpolis/opdm-etl',
    license='GPLv3',
    description=__import__('opdmetl').__description__.strip(),
    classifiers=[
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Development Status :: 3 - Alpha',
        'Operating System :: OS Independent',
    ],
    long_description=read_file('README.rst'),
    test_suite="runtests.runtests",
    zip_safe=False,
    tests_require=['fake-factory'],
    install_requires=[
        "cssselect",
        "pandas",
        "rdflib",
        "rdflib-jsonld",
        "requests",
        "SPARQLWrapper",
        "elasticsearch",
    ],
)
