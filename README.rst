opdm-etl
========

.. image:: https://travis-ci.org/openpolis/opdm-etl.svg?branch=master
  :target: https://travis-ci.org/openpolis/opdm-etl

.. image:: https://coveralls.io/repos/openpolis/opdm-etl/badge.svg?branch=master&service=github
  :target: https://coveralls.io/github/openpolis/opdm-etl?branch=master


Welcome to the documentation for opdm-etl!

**opdmetl** is a minimal ETL framework to use when importing data from a variety of sources.
See internal documentation for more details.

Installation
------------
To install ``opdm-etl`` you may use pip, pointing to the gitlab repository:

.. code-block:: bash

    pip install -e git+git@gitlab.depp.it:openpolis/opdm/opdm-etl.git#egg=opdm-etl


Running the Tests
-----------------

Set up the tests with:

.. code-block:: bash

    pip install -r tests_requirements.txt
    python setup.py install

You can run the tests with via:

.. code-block:: bash

    python setup.py test

or:

.. code-block:: bash

    python runtests.py


Tutorial
--------

This is a quick tutorial on how to use the `opdmetl` module in order to fetch
data from a SQL source and store them into a CSV destination.

The ETL process is performed by invoking the `etl()` method on a `opdmetl.ETL` instance.
The `etl()` method is a shortcut to the sequence `opdmetl.ETL::extract().transform().load()`,
which is possible, as each method returns a pointer to the `opdmetl.ETL` instance.

When the `opdmetl.ETL` instance invokes the `opdmetl.ETL::extract()` method, it invokes the corresponging
`opdmetl.Extractor::extract()` method of the *extractor*. The method extracts the data from the source
into the `opdmetl.ETL::original_data` attribute of the opdmetl.`ETL` instance.

The `opdmetl.ETL::transform()` method is overridden in the instance and may be used to apply
custom data transformation, before the loading phase.
The data from `opdmetl.ETL::original_data` are then transformed into `opdmetl.ETL::processed_data`.

The `opdmetl.ETL::load()` method invokes the `opdmetl.Loader::load()` method storing the data from
`opdmetl.ETL::processed_data` into the defined destination.

The package provides a series of simple Extractors and Loaders, derived from common abstract classes.

Extractors:

- CSVExctractor(RemoteExtractor) - extracts data from a remote CSV
- ZIPCSVExctractor(CSVExctractor) - extracts data from a remote zipped CSV (extends the CSVExtractor)
- HTMLParserExtractor(RemoteExtractor) - extracts data from a remote HTML page (requires parse override)
- SparqlExtractor(RemoteExtractor) - extracts data from a remote SPARQL endpoint
- SqlExtractor(Extractor) - extracts data from a RDBMS

Loaders:

- CSVLoader(Loader) - loads file into a CSV
- ESLoader(Loader) - loads file into an ES instance

The `opdmetl.ETL` abstract class is defined in the `__init__.py` file of the `opdmetl` package.

As an example, here is how to extract data from a sql source into a CSV file

.. code-block:: python

    from opdmetl import ETL
    from opdmetl.extractors import SqlExtractor
    from opdmetl.loaders import CSVLoader

    class MySqlETL(ETL):

        def transform(self):
            od = self.original_data

            od = some_transformation(od)

            self.processed_data = od
            return self


    etl = MySqlETL(
        extractor=SqlExtractor(
            conn_url="mysql://root:@localhost:3306/db_production",
            sql="select * from projects"
        ),
        loader=CSVLoader(
            csv_path="/Users/gu/Workspace/my-import/data-processed/",
            label='projects'
        )
    )
    etl.etl()


Extractors (and Loaders) may be easily extended within the projects using the `opdm-etl` package.
As an example, consider the following example, extending the `opdmetl.HTMLParserExtractor`:

.. code-block:: python

    class GovernoItParserExtractor(HTMLParserExtractor):

        def parse(self, html_content):
            list_tree = html.fromstring(html_content)
            items = []
            for e in CSSSelector('div.content div.box_text a')(list_tree):
                item_name = e.text_content().strip()
                item_url = e.get('href').strip()
                item_page = requests.get(item_url)
                item_tree = html.fromstring(item_page.content)
                item_par = CSSSelector('div.content div.field')(item_tree)[0]
                item_charge = CSSSelector('blockquote p')(item_par)[0].text_content().strip()
                item_descr = " ".join([
                  e.text_content() for e in CSSSelector('p')(item_par)[1:] if\
                     e.text_content() is not None
                ])
                items.append({
                    'nome': item_name,
                    'url': item_url,
                    'incarico': item_charge,
                    'descrizione': item_descr
                })

                if self.etl.verbosity:
                    print item_name

            return items

The Extractors and Loaders defined in the package requires a few packages, in order to provide minimal
functionalities::

    cssselect
    lxml
    pandas
    rdflib
    rdflib-jsonld
    requests
    SPARQLWrapper
    elasticsearch

License
-------

Copyright (C) 2017  Guglielmo Celata

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
