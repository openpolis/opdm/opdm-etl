Install
=======

``opdmetl`` is compatible with python 2 and 3. Its requirements are described in the ``requirements.txt`` file
and may vary, according to the kind of data sources and datastores used.

Installation within a project will amount to:

.. code-block:: bash

    pip install -e git@gitlab.depp.it:openpolis/opdm/opdm-etl.git=opdm-etl

If you're using ``pip``.


To develop on a local machine, install by pointing to the source path

.. code-block:: bash

    pip install -e "/Users/gu/Workspace/opdm-etl"#egg=opdm-etl
