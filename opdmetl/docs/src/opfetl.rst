API
===
Documentation of the `opdmetl` module and its submodules:
`extractors`, `loaders`, and `utils`.

opdmetl
------
.. automodule:: opdmetl
    :members:
    :undoc-members:
    :show-inheritance:

opdmetl.extractors
-----------------
.. automodule:: opdmetl.extractors
    :members:
    :undoc-members:
    :show-inheritance:


opdmetl.loaders
--------------
.. automodule:: opdmetl.loaders
    :members:
    :undoc-members:
    :show-inheritance:


