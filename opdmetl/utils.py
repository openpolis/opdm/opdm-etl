#
# helper functions
#

def region_names_with_article(region_name):
    """Returns region name with correct article (in italian)

    Args:
       region_name (str):  The region name.

    Returns:
       string:  The region name, with the correct italian article.
    """
    if region_name.upper() in ['PIEMONTE', 'TRENTINO-ALTO ADIGE', 'VENETO',
                               'FRIULI-VENEZIA GIULIA', 'LAZIO', 'MOLISE']:
        article = 'del '
    elif region_name.upper() == 'ABRUZZO':
        article = 'dell\''
    else:
        article = 'della '
    return '{}{}'.format(article, region_name)


def post_full_description(region_name, description):
    """Returns full description of a given role

    The full denomination of a role (assessore, consigliere, presidente), along
    with the region name are computed and returned

    Args:
       region_name (str):  The region name.
       description (str):  The description of the role

    Returns:
       string:  The full description, with the correct italian article.
    """
    if description.lower() in ['assessore',
                               'assessore non di origine elettiva']:
        suffix = "della Giunta regionale {}".format(
            region_names_with_article(region_name))
    elif description.lower() in ['presidente della regione',
                                 'vicepresidente della regione di origine '
                                 'elettiva']:
        description = description.replace("regione",
                                          "Regione {}".format(region_name))
        suffix = ""
    else:
        suffix = "della Regione {}".format(region_name)
    if suffix:
        return "{} {}".format(description, suffix)
    else:
        return "{}".format(description)


def post_organs_name(post_description):
    """Returns a list of institutional organs to which the role is linked to

    Args:
       post_description (str):  The description of the role

    Returns:
       list:  The organs, as list of strings ['Giunta', 'Consiglio',
       'Presidente']
    """
    organs = []
    if post_description.lower() in ['assessore',
                                    'assessore non di origine elettiva',
                                    'vicepresidente della regione di origine '
                                    'elettiva',
                                    'vicepresidente della regione non di '
                                    'origine elettiva',
                                    'presidente della regione']:
        organs.append('Giunta')
    if post_description.lower() in ['consigliere',
                                    'consigliere - candidato presidente',
                                    'presidente del consiglio',
                                    'vicepresidente del consiglio',
                                    'segretario del consiglio',
                                    'presidente della regione']:
        organs.append('Consiglio')
    if post_description.lower() in ['presidente della regione']:
        organs.append('Presidente')
    return organs
