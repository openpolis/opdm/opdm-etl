# -*- coding: utf-8 -*-
import django.test
from unittest.mock import Mock, patch


class ETLTest(django.test.TestCase):

    @classmethod
    def setUpClass(cls):
        super(ETLTest, cls).setUpClass()
        cls.mock_get_patcher = patch('opdmetl.extractors.requests.get')
        cls.mock_get = cls.mock_get_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.mock_get_patcher.stop()
        super(ETLTest, cls).tearDownClass()